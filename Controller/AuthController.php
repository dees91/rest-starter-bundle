<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 19/08/16
 * Time: 16:10
 */

namespace Deesoft\RestStarterBundle\Controller;

use Deesoft\RestStarterBundle\Entity\User;
use Deesoft\RestStarterBundle\HttpFoundation\ApiResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthController
 * @package Deesoft\RestStarterBundle\Controller
 *
 * @Route("/auth")
 */
class AuthController extends RestController {

	/**
	 * @ApiDoc(
	 *  section="Auth",
	 *  description="Fake auth"
	 * )
	 *
	 * @Route("/fake", name="auth_fake")
	 * @Method("GET")
	 *
	 * @param Request $request
	 *
	 * @return ApiResponse
	 */
	public function fakeAuthAction(Request $request) {
		$userRepository = $this->getEm()->getRepository('RestStarterBundle:User');
		$users = $userRepository->findAll();
		if (count($users)) {
			$user = $users[0];
		} else {
			$user = new User();
			$user->setEmail('kakarotto.bazin@gmail.com');
			$user->setPlainPassword('mediatech');
			$this->get('rest.user_manager')->updateUser($user);
		}

		return ApiResponse::fromSuccessArray([
			'accessToken' => $this->getJwt()->createAccessToken($user),
			'refreshToken' => $this->getJwt()->createRefreshToken($user)
		]);
	}
}