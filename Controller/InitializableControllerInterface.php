<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 18/08/16
 * Time: 22:10
 */

namespace Deesoft\RestStarterBundle\Controller;


use Symfony\Component\HttpFoundation\Request;

interface InitializableControllerInterface {
	public function __init(Request $request);
}