<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 16/08/16
 * Time: 15:27
 */

namespace Deesoft\RestStarterBundle\Controller;


use AppBundle\Util\FormUtil;
use Deesoft\RestStarterBundle\DependencyInjection\JWTManager;
use Deesoft\RestStarterBundle\DependencyInjection\Serializer;
use Deesoft\RestStarterBundle\DependencyInjection\UserProvider;
use Deesoft\RestStarterBundle\Entity\User;
use Deesoft\RestStarterBundle\HttpFoundation\ApiResponse;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RestController extends Controller implements InitializableControllerInterface {

	/**
	 * @var Request $request
	 */
	protected $request;

	/**
	 * @var JWTManager
	 */
	protected $jwt;

	/**
	 * @var Serializer
	 */
	protected $serializer;

	/**
	 * @param Request $request
	 */
	public function __init( Request $request ) {
		$this->request = $request;
		$this->jwt = $this->get('rest.jwt');
		$this->serializer = $this->get('rest.serializer');
		$user = $this->jwt->decodeAccessToken($this->getToken($request));
		if ($user instanceof User) {
			$this->get('security.token_storage')->getToken()->setUser($user);
		}
	}

	/**
	 * @return EntityManager
	 */
	protected function getEm() {
		return $this->get('doctrine.orm.entity_manager');
	}

	/**
	 * @return JWTManager
	 */
	protected function getJwt() {
		return $this->jwt;
	}

	/**
	 * @return UserProvider
	 */
	protected function getUserProvider() {
		return $this->get('rest.user_provider');
	}

	/**
	 * @return bool
	 */
	public function isAuthenticated() {
		return $this->getUser() instanceof User;
	}

	/**
	 * @return Serializer
	 */
	public function getSerializer() {
		return $this->serializer;
	}

	/**
	 * @param Request $request
	 *
	 * @return array|mixed|string
	 */
	protected function getToken(Request $request) {
		$token = $request->headers->get('X-JWT-Authorization');
		if (!$token) {
			$token = $request->get('jwt-bearer');
		}

		return str_replace('Bearer ', '', $token);
	}

	/**
	 * @param Request $request
	 * @param         $type
	 * @param null    $data
	 * @param array   $options
	 *
	 * @return ApiResponse
	 */
	protected function addSimple(Request $request, $type, $data = null, array $options = []) {
		$form = $this->createForm($type, $data, $options);

		$form->submit(FormUtil::decode($request));
		if ($form->isValid()) {
			$em = $this->getEm();
			$em->persist($data);
			$em->flush();

			return ApiResponse::fromSuccessArray([]);
		}

		return ApiResponse::fromErrorForm($form);
	}
}