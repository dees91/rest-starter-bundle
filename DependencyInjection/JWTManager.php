<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 18/08/16
 * Time: 21:24
 */

namespace Deesoft\RestStarterBundle\DependencyInjection;


use Deesoft\RestStarterBundle\Entity\User;
use Deesoft\RestStarterBundle\Model\JwtSerializable;
use Firebase\JWT\JWT;
use Symfony\Component\Security\Core\User\UserInterface;

class JWTManager {

	const TYPE_ACCESS = 'access';
	const TYPE_REFRESH = 'refresh';

	/**
	 * @var Serializer
	 */
	private $serializer;

	/**
	 * @var string
	 */
	protected $accessSecret = 'M^wDJD;RTpqrcH{%64emqi9t7TF(k;86';

	/**
	 * @var string
	 */
	protected $refreshSecret = 'e=k.uxMp2h)fq63*HJ8r{k4kEK8KDh#B';

	/**
	 * JWTManager constructor.
	 *
	 * @param Serializer $serializer
	 */
	public function __construct( Serializer $serializer ) {
		$this->serializer = $serializer;
	}

	/**
	 * @param User $user
	 * @param int  $minutes
	 *
	 * @return string
	 */
	public function createAccessToken(User $user, $minutes = 60) {
		$payload = [
			'exp' => time() + (60 * $minutes),
			'user' => $this->createUserPayload($user),
			'tokenType' => self::TYPE_ACCESS
		];

		return JWT::encode($payload, $this->accessSecret);
	}

	/**
	 * @param User $user
	 * @param int  $minutes
	 *
	 * @return string
	 */
	public function createRefreshToken(User $user, $minutes = 60) {
		$payload = [
			'exp' => time() + (60 * $minutes),
			'user' => $this->createUserPayload($user),
			'tokenType' => self::TYPE_REFRESH
		];

		return JWT::encode($payload, $this->refreshSecret);
	}

	/**
	 * @param $token
	 *
	 * @return User|null
	 */
	public function decodeAccessToken($token) {
		try {
			if (($payload = JWT::decode($token, $this->accessSecret, ['HS256'])) == true) {
				$payload = (array)$payload;
				if (isset($payload['tokenType'], $payload['user']) && $payload['tokenType'] == self::TYPE_ACCESS) {
					return $this->serializer->deserialize(json_encode($payload['user']), 'Deesoft\RestStarterBundle\Entity\User');
				}
			}
		} catch (\Exception $e) {}

		return null;
	}

	/**
	 * @param $token
	 *
	 * @return User|null
	 */
	public function decodeRefreshToken($token) {
		try {
			if (($payload = JWT::decode($token, $this->refreshSecret, ['HS256'])) == true) {
				$payload = (array)$payload;
				if (isset($payload['tokenType'], $payload['user']) && $payload['tokenType'] == self::TYPE_REFRESH) {
					return $this->serializer->deserialize(json_encode($payload['user']), 'Deesoft\RestStarterBundle\Entity\User');
				}
			}
		} catch (\Exception $e) {}

		return null;
	}

	protected function createUserPayload(UserInterface $user) {
		if ($user instanceof JwtSerializable) {
			return $user->jwtSerialize();
		}

		return $this->serializer->toArray($user, 'jwt');
	}
}