<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 18/08/16
 * Time: 21:29
 */

namespace Deesoft\RestStarterBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class RestStarterExtension extends Extension {

	/**
	 * Loads a specific configuration.
	 *
	 * @param array            $configs   An array of configuration values
	 * @param ContainerBuilder $container A ContainerBuilder instance
	 *
	 * @throws \InvalidArgumentException When provided tag is not defined in this extension
	 */
	public function load( array $configs, ContainerBuilder $container ) {
		$path     = __DIR__ . '/../Resources/config';
		$filename = 'services.yml';
		$loader   = new YamlFileLoader(
			$container,
			new FileLocator($path)
		);

		if (realpath("{$path}/{$filename}")) {
			$loader->load($filename);
		}
	}
}