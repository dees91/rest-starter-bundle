<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 23/08/16
 * Time: 11:21
 */

namespace Deesoft\RestStarterBundle\DependencyInjection;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use \JMS\Serializer\Serializer as JmsSerializer;

class Serializer {

	/**
	 * @var JmsSerializer
	 */
    protected $serializer;

	/**
	 * Serializer constructor.
	 *
	 * @param JmsSerializer $serializer
	 */
	public function __construct( JmsSerializer $serializer ) {
		$this->serializer = $serializer;
	}

	public function toArray($data, $group = 'list') {
		return $this->serializer->toArray($data, SerializationContext::create()->setGroups(['list']));
	}

	public function toJson($data, $group = 'list') {
		return $this->serializer->serialize($data, 'json', SerializationContext::create()->setGroups(['list']));
	}

	public function deserialize($data, $type, DeserializationContext $context = null) {
		return $this->serializer->deserialize($data, $type, 'json', $context);
	}
}