<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 23/08/16
 * Time: 13:54
 */

namespace Deesoft\RestStarterBundle\DependencyInjection;


use Deesoft\RestStarterBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserManager {

	/**
	 * @var EncoderFactoryInterface
	 */
	protected $encoderFactory;

	/**
	 * @var EntityManager
	 */
	protected $entityManager;

	/**
	 * UserManager constructor.
	 *
	 * @param EncoderFactoryInterface $encoderFactory
	 * @param EntityManager           $entityManager
	 */
	public function __construct( EncoderFactoryInterface $encoderFactory, EntityManager $entityManager ) {
		$this->encoderFactory = $encoderFactory;
		$this->entityManager  = $entityManager;
	}

	/**
	 * Updates a user.
	 *
	 * @param User $user
	 * @param bool $andFlush
	 *
	 * @return void
	 */
	public function updateUser(User $user, $andFlush = true)
	{
		$this->updateCanonicalFields($user);
		$this->updatePassword($user);

		$this->entityManager->persist($user);
		if ($andFlush) {
			$this->entityManager->flush();
		}
	}

	/**
	 * Updates a user password if a plain password is set.
	 *
	 * @param User $user
	 *
	 * @return void
	 */
	protected function updatePassword(User $user)
	{
		if (0 !== strlen($password = $user->getPlainPassword())) {
			$encoder = $this->encoderFactory->getEncoder($user);
			$user->setPassword($encoder->encodePassword($password, $user->getSalt()));
			$user->eraseCredentials();
		}
	}

	/**
	 * Updates the canonical username and email fields for a user.
	 *
	 * @param User $user
	 *
	 * @return void
	 */
	protected function updateCanonicalFields(User $user)
	{
		$user->setEmail($this->canonicalize($user->getEmail()));
	}

	protected function canonicalize($string)
	{
		return mb_convert_case($string, MB_CASE_LOWER, mb_detect_encoding($string));
	}
}