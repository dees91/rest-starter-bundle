<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 23/08/16
 * Time: 14:24
 */

namespace Deesoft\RestStarterBundle\DependencyInjection;


use Deesoft\RestStarterBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class UserProvider {

	/**
	 * @var TokenStorage
	 */
	protected $tokenStorage;

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * UserProvider constructor.
	 *
	 * @param TokenStorage  $tokenStorage
	 * @param EntityManager $em
	 */
	public function __construct( TokenStorage $tokenStorage, EntityManager $em ) {
		$this->tokenStorage = $tokenStorage;
		$this->em           = $em;
	}

	/**
	 * @return User|null
	 */
	public function getUserFromDB() {
		return $this->em->getRepository('RestStarterBundle:User')->find($this->getUserFromJwt()->getId());
	}

	/**
	 * @return User|null
	 */
	public function getUserFromJwt() {
		return $this->tokenStorage->getToken()->getUser();
	}
}