<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 22/08/16
 * Time: 13:40
 */

namespace Deesoft\RestStarterBundle\Entity;

use AppBundle\Entity\Mixin\Timestampable;
use AppBundle\Util\StringUtil;
use Deesoft\RestStarterBundle\Model\JwtSerializable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package Deesoft\RestStarterBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="api_users")
 */
class User implements UserInterface, JwtSerializable {

	const ROLE_USER = 'ROLE_USER';
	use Timestampable;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 *
	 * @var int
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=60, unique=true)
	 *
	 * @var string
	 */
	protected $email;

	/**
	 * @ORM\Column(type="string", length=255)
	 *
	 * @var string
	 */
	protected $password;

	/**
	 * @var string
	 */
	protected $plainPassword;

	/**
	 * @ORM\Column(name="salt", type="string", length=255)
	 *
	 * @var string
	 */
	protected $salt;

	/**
	 * @ORM\Column(type="string")
	 *
	 * @var array
	 */
	protected $roles;

	/**
	 * @ORM\OneToMany(targetEntity="Deesoft\RestStarterBundle\Entity\UserMeta", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
	 *
	 * @var UserMeta[]|ArrayCollection
	 */
	protected $metas;

	/**
	 * User constructor.
	 */
	public function __construct() {
		$this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
		$this->roles = json_encode([self::ROLE_USER]);
		$this->metas = new ArrayCollection();
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPlainPassword() {
		return $this->plainPassword;
	}

	/**
	 * @param mixed $plainPassword
	 */
	public function setPlainPassword( $plainPassword ) {
		$this->plainPassword = $plainPassword;
	}

	/**
	 * @param array $roles
	 */
	public function setRoles( array $roles ) {
		$this->roles = json_encode($roles);
	}

	public function getRoles() {
		return json_decode($this->roles, true);
	}

	/**
	 * @param string $role
	 */
	public function addRole($role) {
		$roles = $this->getRoles();
		if (!in_array($role, $roles)) {
			$roles[] = $role;
			$this->setRoles($roles);
		}
	}

	/**
	 * @param string $role
	 */
	public function removeRole($role) {
		$roles = $this->getRoles();
		if (in_array($role, $roles)) {
			unset($roles[array_search($role, $roles)]);
			$this->setRoles($roles);
		}
	}

	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword( $password ) {
		$this->password = $password;
	}

	public function getSalt() {
		return $this->salt;
	}

	public function getUsername() {
		return $this->email;
	}

	public function eraseCredentials() {
		$this->plainPassword = null;
	}

	public function jwtSerialize() {
		return [
			'id' => $this->id,
			'email' => $this->email
		];
	}

	/**
	 * @param UserMeta $userMeta
	 *
	 * @return $this
	 */
	public function addMeta(UserMeta $userMeta)
	{
		if (!$this->metas->containsKey($userMeta->getKey())) {
			$this->metas->set($userMeta->getKey(), $userMeta);
			$userMeta->setUser($this);
		}

		return $this;
	}

	/**
	 * @param UserMeta $userMeta
	 *
	 * @return $this
	 */
	public function removeMeta(UserMeta $userMeta)
	{
		if ($this->metas->containsKey($userMeta->getKey())) {
			$this->metas->remove($userMeta->getKey());
			$userMeta->setUser(null);
		}

		return $this;
	}

	/**
	 * @return UserMeta[]|ArrayCollection
	 */
	public function getMetas() {
		foreach ($this->metas as $key => $meta) {
			if (!$this->metas->contains($meta->getKey())) {
				$this->metas->removeElement($meta);
				$this->metas->set($meta->getKey(), $meta);
			}
		}

		return $this->metas;
	}

	/**
	 * @param UserMeta[]|ArrayCollection $metas
	 */
	public function setMetas( $metas ) {
		$this->metas = new ArrayCollection();
		foreach ($metas as $meta) {
			$this->addMeta($meta);
		}
	}

	/**
	 * @param $key
	 *
	 * @return array|mixed|null
	 */
	public function getMeta($key) {
		if ($this->getMetas()->containsKey($key)) {
			return $this->getMetas()->get($key);
		}

		return null;
	}

	/**
	 * @param      $key
	 * @param null $defaultValue
	 *
	 * @return null|string
	 */
	public function getMetaValue($key, $defaultValue = null) {
		$meta = $this->getMeta($key);

		if ($meta instanceof UserMeta) {
			return StringUtil::getValuable($meta->getValue());
		}

		return $defaultValue;
	}
}