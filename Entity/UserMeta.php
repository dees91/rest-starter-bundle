<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 24/08/16
 * Time: 12:30
 */

namespace Deesoft\RestStarterBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="api_users_meta")
 */
class UserMeta {

	const INIT_BALANCE = 'init_balance';
	const INIT_DATE = 'init_date';

	/**
	 * @ORM\ManyToOne(targetEntity="Deesoft\RestStarterBundle\Entity\User", fetch="EAGER", inversedBy="metas")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
	 *
	 * @var User
	 */
	protected $user;

	/**
	 * @var string
	 *
	 * @ORM\Id
	 * @ORM\Column(name="`key`", type="string", nullable=false, unique=true)
	 */
	protected $key;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(name="`value`", type="text", nullable=true)
	 */
	protected $value;

	/**
	 * UserMeta constructor.
	 *
	 * @param string      $key
	 * @param null|string $value
	 */
	public function __construct( $key = null, $value = null ) {
		$this->key   = $key;
		$this->value = $value;
	}

	/**
	 * @return User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser( $user ) {
		$this->user = $user;
	}

	/**
	 * @return string
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @param string $key
	 */
	public function setKey( $key ) {
		$this->key = $key;
	}

	/**
	 * @return null|string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @param null|string $value
	 */
	public function setValue( $value ) {
		$this->value = $value;
	}
}