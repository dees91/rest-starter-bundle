<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 23/08/16
 * Time: 12:35
 */

namespace Deesoft\RestStarterBundle\EventListener;


use Deesoft\RestStarterBundle\Controller\RestController;
use Deesoft\RestStarterBundle\HttpFoundation\ApiError;
use Deesoft\RestStarterBundle\HttpFoundation\ApiResponse;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class AnnotationListener {

	/**
	 * @var AnnotationReader
	 */
	protected $reader;

	/**
	 * AnnotationListener constructor.
	 *
	 * @param AnnotationReader $reader
	 */
	public function __construct( AnnotationReader $reader ) {
		$this->reader = $reader;
	}

	public function onKernelController(FilterControllerEvent $event) {
		$controller = $event->getController();

		if (!is_array($controller)) {
			return;
		}

		list($controllerObject, $methodName) = $controller;

		$demoAnnotation = 'Deesoft\RestStarterBundle\Annotation\JwtSecure';

		$controllerReflectionObject = new \ReflectionObject($controllerObject);
		$reflectionMethod = $controllerReflectionObject->getMethod($methodName);
		$methodAnnotation = $this->reader->getMethodAnnotation($reflectionMethod, $demoAnnotation);

		if ($methodAnnotation && $controllerObject instanceof RestController && !$controllerObject->isAuthenticated()) {
			$event->setController(
				function() {
					return ApiResponse::fromErrorCode(ApiError::UNAUTHORIZED);
				}
			);
		}
	}
}