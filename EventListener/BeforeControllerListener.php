<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 18/08/16
 * Time: 22:12
 */

namespace Deesoft\RestStarterBundle\EventListener;


use Deesoft\RestStarterBundle\Controller\InitializableControllerInterface;
use Symfony\Bundle\WebProfilerBundle\Controller\ExceptionController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class BeforeControllerListener {

	/**
	 * @param FilterControllerEvent $event
	 */
	public function onKernelController(FilterControllerEvent $event) {
		$controller = $event->getController();
		if (!is_array($controller)) {
			return;
		}
		$controllerObject = $controller[0];

		if ($controllerObject instanceof ExceptionController) {
			return;
		}
		if ($controllerObject instanceof InitializableControllerInterface) {
			$controllerObject->__init($event->getRequest());
		}
	}
}