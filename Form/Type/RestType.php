<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 16/08/16
 * Time: 16:34
 */

namespace Deesoft\RestStarterBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RestType
 * @package Deesoft\BudgetBundle\Form\Type
 */
abstract class RestType extends AbstractType {

	/**
	 * @return string
	 */
	abstract protected function getEntity();

	/**
	 * @return string
	 */
	public function getName() {
		return null;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => $this->getEntity(),
			'allow_extra_fields' => true,
			'csrf_protection' => false
		]);
	}
}