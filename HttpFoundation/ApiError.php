<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 14/06/16
 * Time: 14:29
 */

namespace Deesoft\RestStarterBundle\HttpFoundation;


class ApiError {
	const VALIDATION_FAILED = 400;
	const INTERNAL_ERROR = 500;
	const UNAUTHORIZED = 401;
}