<?php
/**
 * @author Piotr Krawczyk
 * Date: 30.07.15
 */

namespace Deesoft\RestStarterBundle\HttpFoundation;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiException extends HttpException {

	protected $errorCode;

	public function __construct($message, $errorCode = ApiError::INTERNAL_ERROR, $statusCode = Response::HTTP_BAD_REQUEST, array $headers = array(), $code = 0)
	{
		$this->errorCode = $errorCode;
		parent::__construct($statusCode, $message, null, $headers, $code);
	}

	public static function fromMessage($message, $statusCode = Response::HTTP_BAD_REQUEST) {
		return new static($message, ApiError::INTERNAL_ERROR, $statusCode);
	}

	public static function fromCode($code, $statusCode = Response::HTTP_BAD_REQUEST) {
		return new static(null, $code, $statusCode);
	}

	/**
	 * @return int|null
	 */
	public function getErrorCode() {
		return $this->errorCode;
	}
}