<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 30/05/16
 * Time: 14:10
 */

namespace Deesoft\RestStarterBundle\HttpFoundation;


use AppBundle\Util\TranslationUtil;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiResponse extends JsonResponse {

	const FROM_SUCCESS_ARRAY = 1;
	const FROM_ERROR_FORM = 2;
	const FROM_API_EXCEPTION = 3;
	const FROM_EXCEPTION = 4;
	const FROM_ERROR_CODE = 5;

	const DEFAULT_RESPONSE = [
		'status' => true,
		'data' => null,
		'errors' => []
	];
	const DEFAULT_PARAMS = [
		'status' => Response::HTTP_OK,
		'response' => [],
		'headers' => []
	];

	public function __construct($type, array $params) {
		$params = array_merge(self::DEFAULT_PARAMS, $params);

		switch ($type) {
			case self::FROM_ERROR_FORM:
				$form_errors = $this->getFormErrors($params['form']);

				$errors = [];
				foreach ($form_errors as $key => $value) {
					$errors[] = [
						'code' => ApiError::VALIDATION_FAILED,
						'message' => $value
					];
				}

				$params = array_merge($params, [
					'response' => array_merge(self::DEFAULT_RESPONSE, [
						'status' => false,
						'errors' => $errors
					])
				]);
				break;
			case self::FROM_ERROR_CODE:
			case self::FROM_API_EXCEPTION:
			case self::FROM_EXCEPTION:
				$params = array_merge($params, [
					'response' => array_merge(self::DEFAULT_RESPONSE, [
						'status' => false,
						'errors' => [$params['error']]
					])
				]);
				break;
			default: break;
		}
		parent::__construct($params['response'], $params['status'], array_merge($params['headers'], [
			'Access-Control-Allow-Origin' => '*',
			'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept'
		]));
	}

	public static function fromSuccessArray(array $_data, $httpCode = Response::HTTP_OK, $headers = []) {
		return new static(self::FROM_SUCCESS_ARRAY, [
			'status' => $httpCode,
			'response' => array_merge(self::DEFAULT_RESPONSE, [
				'data' => $_data
			]),
			'headers' => $headers
		]);
	}

	public static function fromErrorForm(Form $form, $headers = []) {
		return new static(self::FROM_ERROR_FORM, [
			'status' => Response::HTTP_BAD_REQUEST,
			'headers' => $headers,
			'form' => $form
		]);
	}

	public static function fromApiException(ApiException $exception, $locale = 'pl') {
		$message = $exception->getMessage() ? $exception->getMessage() : ('error.code.' . $exception->getErrorCode());
		return new static(self::FROM_API_EXCEPTION, [
			'status' => $exception->getStatusCode(),
			'error' => [
				'code' => $exception->getErrorCode(),
				'message' => TranslationUtil::trans($message, $locale)
			]
		]);
	}

	public static function fromException(\Exception $exception, $httpCode = Response::HTTP_INTERNAL_SERVER_ERROR, $locale = 'pl') {
		return new static(self::FROM_EXCEPTION, [
			'status' => $httpCode,
			'error' => [
				'code' => $httpCode,
				'message' => TranslationUtil::trans($exception->getMessage(), $locale)
			]
		]);
	}

	public static function fromErrorCode($errorCode, $httpCode = Response::HTTP_BAD_REQUEST, $locale = 'pl') {
		return new static(self::FROM_ERROR_CODE, [
			'status' => $httpCode,
			'error' => [
				'code' => $errorCode,
				'message' => TranslationUtil::trans('error.code.' . $errorCode, $locale)
			]
		]);
	}

	/**
	 * @param Form $form
	 * @return array
	 */
	protected function getFormErrors(Form $form) {
		$errors = [];

		if ($form instanceof Form) {
			foreach ($form->getErrors() as $error) {
				$errors[] = $error->getMessage();
			}

			foreach ($form->all() as $key => $child) {
				if ($err = $this->getFormErrors($child)) {
					if (is_array($err)) {
						$err = array_unique($err);
						if (is_array($err) && count($err) == 1) {
							$err = $err[0];
						}
					}
					$errors[$key] = $err;
				}
			}
		}

		return $errors;
	}
}