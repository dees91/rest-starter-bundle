<?php
/**
 * Created by PhpStorm.
 * User: dees91
 * Date: 23/08/16
 * Time: 11:16
 */

namespace Deesoft\RestStarterBundle\Model;


interface JwtSerializable {
	public function jwtSerialize();
}